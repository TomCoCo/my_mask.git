自定义调用torch_version的mask-rcnn项目

这是自实现的文档说明: [pytorch中创建maskrcnn模型](https://blog.csdn.net/qq_37293230/article/details/138127095)

注意!!!
启动方法有2个,一个在run模块,一个在main模块
run模块是调用pytorch官方实现的maskrcnn
main模块调用的是model目录下自实现的maskrcnn(没写完呢)(学习参考使用)
二者使用同一个config(使用main运行时会忽略大部分的参数,因为参数写在模型上了,为了阅读学习清晰)

启动报错可能是缺一个out目录,手动建立一下就好

注意!!!!
可以直接修改
torchvision\models\detection\roi_heads.py 716行的代码,使之nms与分类无关
keep = box_ops.batched_nms(boxes, scores, labels, self.nms_thresh)->
keep = box_ops.batched_nms(boxes, scores, torch.ones_like(labels), self.nms_thresh)
默认调用实现了一个默认的nms之后再进行一次类型无关的nms


import torch
import torchvision

# 检查PyTorch是否支持CUDA
if torch.cuda.is_available():
    print("PyTorch可以使用CUDA")
    print("CUDA设备数量：", torch.cuda.device_count())
    print("当前CUDA设备：", torch.cuda.current_device())
    print("CUDA设备名称：", torch.cuda.get_device_name(0))
    print("CUDA版本：", torch.version.cuda)
else:
    print("PyTorch无法使用CUDA")

# 下载voc数据集的
# torchvision.datasets.VOCSegmentation("./dataset/voc",year="2012",image_set="trainval",download=False)
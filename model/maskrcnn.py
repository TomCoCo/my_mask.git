import copy
import math
from collections import OrderedDict

import torch
import torch.nn as nn
from torch import Tensor
from torchvision.models import resnet50, ResNet50_Weights
from torchvision.models.detection._utils import BalancedPositiveNegativeSampler, Matcher
from torchvision.models.detection.faster_rcnn import FastRCNNConvFCHead
from torchvision.models.detection.mask_rcnn import MaskRCNNHeads
from torchvision.models.detection.roi_heads import project_masks_on_boxes
from torchvision.models.detection.rpn import concat_box_prediction_layers
from torchvision.ops import Conv2dNormActivation, MultiScaleRoIAlign
from torchvision.ops import boxes as box_ops


class TransformRcnn(nn.Module):
    """
    标准化和合并images为同一tensor操作
    """

    def __init__(self):
        super().__init__()

        self.image_mean = [0.485, 0.456, 0.406]
        self.image_std = [0.229, 0.224, 0.225]
        self.min_size = 800
        self.max_size = 1333

    def normalize(self, image: Tensor) -> Tensor:
        dtype, device = image.dtype, image.device
        mean = torch.as_tensor(self.image_mean, dtype=dtype, device=device)
        std = torch.as_tensor(self.image_std, dtype=dtype, device=device)
        return (image - mean[:, None, None]) / std[:, None, None]

    def resize(self, image, target):
        """
        当min_size=800,max_size=1333时,示例数据如下
        image 输入形状 [[3,312, 632], [3,490, 564], [3,884, 1494], [3,658, 1008]]
        image 输出形状 [[3,658, 1333], [3,800, 920], [3,788, 1333], [3,800, 1225]]
        过小的数据会被线性插值放大,过大的会被线性插值缩小,masks同样如此(近邻插值).
        缩放比例取决与宽高谁更远离最大/最小值,即保留图像比例缩放图像,宽高更大的那个在800-1333之间
        """
        # 先计算缩放比例
        h, w = image.shape[-2:]
        im_min = min(h, w)
        im_max = max(h, w)
        scale = min(self.min_size / im_min, self.max_size / im_max)

        # 使用双线性插值缩放图像
        image = nn.functional.interpolate(image.unsqueeze(0), scale_factor=scale,
                                          mode="bilinear", align_corners=False)[0]
        # 对mask使用近邻插值方式,保持0,1不变
        masks = target['masks']
        target['masks'] = nn.functional.interpolate(masks[:, None], scale_factor=scale, mode="nearest")[:, 0]
        return image, target

    def batch_images(self, images: list[Tensor], size_divisible: int = 32) -> Tensor:
        """
        将标准化和缩放过的图像,从list合并为同一个tensor list(4)->tensor(4,3,h,w)
        注意,输入的hw可能会不一致,那么此时需要兼容最大尺寸.下面是batch=4时的一个输入尺寸实例
         [[3, 800, 920],[3, 658, 1333],[3, 788, 1333],[3, 800, 1225]]
        那么输出应该形如 Tensor(4,3,800,1344)
        之所以不是1333,而是1344,是因为size_divisible参数,使之对齐到32的整数倍了,这样会更便于后面的计算
        """
        # 先求最大尺寸
        image_size = torch.zeros((len(images), 3), dtype=torch.int)
        for i, image in enumerate(images):
            image_size[i] = torch.as_tensor(image.size(), dtype=torch.int)
        max_size, _ = torch.max(image_size, dim=0)
        batch_img_size = max_size.tolist()
        stride = float(size_divisible)
        batch_img_size[1] = int(math.ceil(float(batch_img_size[1]) / stride) * stride)
        batch_img_size[2] = int(math.ceil(float(batch_img_size[2]) / stride) * stride)
        batch_img_size.insert(0, len(images))
        # 依照最大尺寸,构建tensor
        batched_imgs = images[0].new_full(batch_img_size, 0)
        # 插入所有图像
        for i, img in enumerate(images):
            # 将img的数据copy到batched_imgs的对应位置
            batched_imgs[i, : img.shape[0], : img.shape[1], : img.shape[2]].copy_(img)
        return batched_imgs

    def forward(self, images, targets):
        for i in range(len(images)):
            image = images[i]
            target = targets[i]
            # 先标准化,后resize(插值)
            image = self.normalize(image)
            image, target = self.resize(image, target)
            # 新值覆盖原list的旧值
            images[i] = image
            target[i] = target

        image_sizes = [img.shape[-2:] for img in images]
        images = self.batch_images(images)
        return image_sizes, images, targets


class ResNet50WithFpn(nn.Module):
    """
    使用resnet50构建fpn网络
    """

    def __init__(self):
        super().__init__()
        # 获取骨干网络模型,但是只抽取部分
        backbone = resnet50(weights=ResNet50_Weights.DEFAULT)
        # 需要返回用以构建fpn层的名字,0-3代表输出的dictkey
        self.return_layers = {'layer1': '0', 'layer2': '1', 'layer3': '2', 'layer4': '3'}
        # 创建return_layers的副本
        return_layers_copy = copy.deepcopy(self.return_layers)
        # 存储所有的的层
        self.layers = nn.ModuleDict()
        # 遍历backbone的层,确保可以找到return_layers定义的所有层,并且找到最后一个层就截断了,后面的不需要
        for name, module in backbone.named_children():
            self.layers[name] = module
            if name in return_layers_copy:
                del return_layers_copy[name]
            # 如果指示的层被删光了,说明遍历到最后一个了,跳出循环
            if not return_layers_copy:
                break

        # 定义FPN输入的深度结构,即backbone的4个输出通道数
        in_channels_list = [256, 512, 1024, 2048]
        # 全部转为256深度输出,得到不同尺寸的同样深度的特征图
        out_channels = 256
        # 处理到同一深度1x1卷积层,生成C层
        self.inner_blocks = nn.ModuleList()
        # 对P层再做次3x3的卷积层
        self.layer_blocks = nn.ModuleList()
        for in_channels in in_channels_list:
            # 用以执行处理同一深度的卷积核
            inner_block_module = Conv2dNormActivation(in_channels, out_channels, kernel_size=1, padding=0,
                                                      norm_layer=None, activation_layer=None)
            self.inner_blocks.append(inner_block_module)
            # 使用一个3x3的卷积核再次处理结果,减少上采样的混叠效应
            layer_block_module = Conv2dNormActivation(out_channels, out_channels, kernel_size=3, padding=0,
                                                      norm_layer=None, activation_layer=None)
            self.layer_blocks.append(layer_block_module)

    def forward(self, images):
        # 创建backbone结果集合
        out = OrderedDict()
        for name, module in self.layers.items():
            images = module(images)
            if name in self.return_layers:
                out_name = self.return_layers[name]
                out[out_name] = images

        # 创建1x1卷积,并执行卷积操作
        cx = list()
        for index, inner_block_module in enumerate(self.inner_blocks):
            x = out.get(str(index))
            x = inner_block_module(x)
            cx.append(x)

        # 执行上采样以及合并操作,以及结果再次卷积
        px = list()
        last_inner = cx[-1]
        # 最后一个丢到结果集上,作为p4
        px.append(self.layer_blocks[-1](last_inner))
        for idx in range(len(cx) - 2, -1, -1):
            # 获取当前这个,以及形状
            inner_lateral = cx[idx]
            feat_shape = inner_lateral.shape[-2:]
            # 对上层的那个执行上采样
            inner_top_down = nn.functional.interpolate(last_inner, size=feat_shape)
            # 相加作为P3~P1
            last_inner = inner_lateral + inner_top_down
            px.insert(0, self.layer_blocks[idx](last_inner))

        # 取出最小特征图做一次池化,卷积核1x1,步长2
        tm = nn.functional.max_pool2d(px[-1], kernel_size=1, stride=2, padding=0)
        px.append(tm)

        names = ["0", "1", "2", "3", "pool"]
        # make it back an OrderedDict
        out = OrderedDict([(k, v) for k, v in zip(names, px)])
        return out


class MaskRCNNPredictor(nn.Sequential):
    def __init__(self, in_channels, dim_reduced, num_classes):
        super().__init__(
            OrderedDict(
                [
                    ("conv5_mask", nn.ConvTranspose2d(in_channels, dim_reduced, 2, 2, 0)),
                    ("relu", nn.ReLU(inplace=True)),
                    ("mask_fcn_logits", nn.Conv2d(dim_reduced, num_classes, 1, 1, 0)),
                ]
            )
        )

        for name, param in self.named_parameters():
            if "weight" in name:
                nn.init.kaiming_normal_(param, mode="fan_out", nonlinearity="relu")


class RoIHeads(nn.Module):
    def __init__(self, num_classes):
        super().__init__()
        self.training = True
        # 采样代码,和之前的采样几乎一样,直接复制源码的上来用
        self.proposal_matcher = Matcher(0.5, 0.5, allow_low_quality_matches=False)
        self.fg_bg_sampler = BalancedPositiveNegativeSampler(512, 0.25)

        # 参数featmap_names使用那几个特征图层进行映射,output_size输出的结果尺寸.sampling_ratio,即在输出的的每一个值,使用多少个点作为采样计算
        self.box_roi_pool = MultiScaleRoIAlign(featmap_names=["0", "1", "2", "3"], output_size=7, sampling_ratio=4)
        # 特征提取层,input_size的深度和特征图一致,输出尺寸和RoIAlign一致,执行4次3x3的卷积操作,通道均为256
        self.box_head = FastRCNNConvFCHead((256, 7, 7), [256, 256, 256, 256], [1024],
                                           norm_layer=torch.nn.BatchNorm2d)
        # 此处的1024为box_head的输出深度,得到预测结果
        self.box_predictor = FastRCNNPredictor(1024, num_classes)

        self.mask_roi_pool = MultiScaleRoIAlign(featmap_names=["0", "1", "2", "3"], output_size=14, sampling_ratio=4)
        self.mask_head = MaskRCNNHeads(256, [256, 256, 256, 256], 1, norm_layer=nn.BatchNorm2d)
        self.mask_predictor = MaskRCNNPredictor(256, 256, num_classes)

    def forward(self, features, proposals, image_sizes, targets):
        # 训练期间进行采样
        if self.training:
            """
            proposal: 采样后的proposal
            matched_idx: proposal和gt的index的关联关系,注意,这里的0.存在2种可能,一种是负样本的,一种是真的0,需要结合labels看
            labels: 即proposal对应的分类是哪一个,注意!!这里的0就代表背景,即负样本,是不会出现-1的,因为负样本也是分类的一种,即0分类(背景)
            """
            proposals, matched_idxs, labels, regression_targets = self.select_training_samples(proposals, targets)
        else:
            labels = None
            regression_targets = None
            matched_idxs = None

        # 多层级roiAlign,按ROI的大小,选择特定的层级的featureMap,
        # 执行RoiAlign,得到proposals个featureMap深度的,指定尺寸的,Tensor,形如Tensor(2000,256,7,7)
        box_features = self.box_roi_pool(features, proposals, image_sizes)
        # 提取更多特征,之后接全连接(如输出为256),即每一个ROI,都用一个(256长的数据作为特征)
        box_features = self.box_head(box_features)
        # 预测分类和每个分类上的回归
        class_logits, box_regression = self.box_predictor(box_features)

        # losses 存储训练期间的loss结果
        losses = {}
        # 存储推理期间的推理结果
        result: list[dict[str, torch.Tensor]] = []
        # 计算分类和回归损失
        if self.training:
            loss_classifier, loss_box_reg = self.fastrcnn_loss(class_logits, box_regression, labels, regression_targets)
            losses = {"loss_classifier": loss_classifier, "loss_box_reg": loss_box_reg}
        # 预测阶段,计算分类得分
        else:
            pass

        mask_proposals = []
        pos_matched_idxs = []
        # mask的预测与损失
        if self.training:
            # 得到mask_proposals,以及和gt的关系
            num_images = len(proposals)
            mask_proposals = []
            pos_matched_idxs = []
            for img_id in range(num_images):
                # 取出预测的labels中有对象的index
                pos = torch.where(labels[img_id] > 0)[0]
                mask_proposals.append(proposals[img_id][pos])
                pos_matched_idxs.append(matched_idxs[img_id][pos])
        # 计算得分,以及取出所有分类中得分最高的作为label
        else:
            boxes, scores, labels = self.postprocess_detections(class_logits, box_regression, proposals, image_sizes)
            num_images = len(boxes)
            for i in range(num_images):
                result.append(
                    {
                        "boxes": boxes[i],
                        "labels": labels[i],
                        "scores": scores[i],
                    }
                )

        mask_features = self.mask_roi_pool(features, mask_proposals, image_sizes)
        mask_features = self.mask_head(mask_features)
        mask_logits = self.mask_predictor(mask_features)

        # 训练阶段,计算loss
        if self.training:
            gt_masks = [t["masks"] for t in targets]
            gt_labels = [t["labels"] for t in targets]
            rcnn_loss_mask = self.maskrcnn_loss(mask_logits, mask_proposals, gt_masks, gt_labels, pos_matched_idxs)
            loss_mask = {"loss_mask": rcnn_loss_mask}
            losses.update(loss_mask)
        # 预测阶段,计算
        else:
            pass
        return result, losses

    def postprocess_detections(
            self,
            class_logits,
            box_regression,
            proposals,
            image_shapes,
    ):
        device = class_logits.device
        num_classes = class_logits.shape[-1]

        boxes_per_image = [boxes_in_image.shape[0] for boxes_in_image in proposals]
        pred_boxes = RPN.decode(box_regression, proposals)

        pred_scores = nn.functional.softmax(class_logits, -1)

        pred_boxes_list = pred_boxes.split(boxes_per_image, 0)
        pred_scores_list = pred_scores.split(boxes_per_image, 0)

        all_boxes = []
        all_scores = []
        all_labels = []
        for boxes, scores, image_shape in zip(pred_boxes_list, pred_scores_list, image_shapes):
            boxes = box_ops.clip_boxes_to_image(boxes, image_shape)

            # create labels for each prediction
            labels = torch.arange(num_classes, device=device)
            labels = labels.view(1, -1).expand_as(scores)

            # remove predictions with the background label
            boxes = boxes[:, 1:]
            scores = scores[:, 1:]
            labels = labels[:, 1:]

            # batch everything, by making every class prediction be a separate instance
            boxes = boxes.reshape(-1, 4)
            scores = scores.reshape(-1)
            labels = labels.reshape(-1)

            # remove low scoring boxes
            inds = torch.where(scores > self.score_thresh)[0]
            boxes, scores, labels = boxes[inds], scores[inds], labels[inds]

            # remove empty boxes
            keep = box_ops.remove_small_boxes(boxes, min_size=1e-2)
            boxes, scores, labels = boxes[keep], scores[keep], labels[keep]

            # non-maximum suppression, independently done per class
            keep = box_ops.batched_nms(boxes, scores, labels, self.nms_thresh)
            # keep only topk scoring predictions
            keep = keep[: self.detections_per_img]
            boxes, scores, labels = boxes[keep], scores[keep], labels[keep]

            all_boxes.append(boxes)
            all_scores.append(scores)
            all_labels.append(labels)

        return all_boxes, all_scores, all_labels

    @staticmethod
    def maskrcnn_loss(mask_logits, proposals, gt_masks, gt_labels, mask_matched_idxs):
        # type: (Tensor, list[Tensor], list[Tensor], list[Tensor], list[Tensor]) -> Tensor
        """
        Args:
            proposals (list[Boxlist])
            mask_logits (Tensor)
            targets (list[Boxlist])

        Return:
            mask_loss (Tensor): scalar tensor containing the loss
        """

        discretization_size = mask_logits.shape[-1]
        labels = [gt_label[idxs] for gt_label, idxs in zip(gt_labels, mask_matched_idxs)]
        mask_targets = [
            project_masks_on_boxes(m, p, i, discretization_size) for m, p, i in
            zip(gt_masks, proposals, mask_matched_idxs)
        ]

        labels = torch.cat(labels, dim=0)
        mask_targets = torch.cat(mask_targets, dim=0)

        # torch.mean (in binary_cross_entropy_with_logits) doesn't
        # accept empty tensors, so handle it separately
        if mask_targets.numel() == 0:
            return mask_logits.sum() * 0

        mask_loss = nn.functional.binary_cross_entropy_with_logits(
            mask_logits[torch.arange(labels.shape[0], device=labels.device), labels], mask_targets
        )
        return mask_loss

    @staticmethod
    def fastrcnn_loss(class_logits, box_regression, labels, regression_targets):

        labels = torch.cat(labels, dim=0)
        regression_targets = torch.cat(regression_targets, dim=0)

        classification_loss = nn.functional.cross_entropy(class_logits, labels)

        # get indices that correspond to the regression targets for
        # the corresponding ground truth labels, to be used with
        # advanced indexing
        sampled_pos_inds_subset = torch.where(labels > 0)[0]
        labels_pos = labels[sampled_pos_inds_subset]
        N, num_classes = class_logits.shape
        box_regression = box_regression.reshape(N, box_regression.size(-1) // 4, 4)

        box_loss = nn.functional.smooth_l1_loss(
            box_regression[sampled_pos_inds_subset, labels_pos],
            regression_targets[sampled_pos_inds_subset],
            beta=1 / 9,
            reduction="sum",
        )
        box_loss = box_loss / labels.numel()

        return classification_loss, box_loss

    def select_training_samples(self, proposals, targets):
        if targets is None:
            raise ValueError("targets should not be None")
        dtype = proposals[0].dtype
        device = proposals[0].device

        gt_boxes = [t["boxes"].to(dtype) for t in targets]
        gt_labels = [t["labels"] for t in targets]

        # append ground-truth bboxes to propos
        proposals = [torch.cat((proposal, gt_box)) for proposal, gt_box in zip(proposals, gt_boxes)]

        # get matching gt indices for each proposal
        matched_idxs, labels = self.assign_targets_to_proposals(proposals, gt_boxes, gt_labels)
        # sample a fixed proportion of positive-negative proposals
        sampled_inds = self.subsample(labels)
        matched_gt_boxes = []
        num_images = len(proposals)
        for img_id in range(num_images):
            img_sampled_inds = sampled_inds[img_id]
            proposals[img_id] = proposals[img_id][img_sampled_inds]
            labels[img_id] = labels[img_id][img_sampled_inds]
            matched_idxs[img_id] = matched_idxs[img_id][img_sampled_inds]

            gt_boxes_in_image = gt_boxes[img_id]
            if gt_boxes_in_image.numel() == 0:
                gt_boxes_in_image = torch.zeros((1, 4), dtype=dtype, device=device)
            matched_gt_boxes.append(gt_boxes_in_image[matched_idxs[img_id]])

        regression_targets = RPN.encode(matched_gt_boxes, proposals)
        return proposals, matched_idxs, labels, regression_targets

    def subsample(self, labels):
        # type: (list[Tensor]) -> list[Tensor]

        sampled_pos_inds, sampled_neg_inds = self.fg_bg_sampler(labels)
        sampled_inds = []
        for img_idx, (pos_inds_img, neg_inds_img) in enumerate(zip(sampled_pos_inds, sampled_neg_inds)):
            img_sampled_inds = torch.where(pos_inds_img | neg_inds_img)[0]
            sampled_inds.append(img_sampled_inds)
        return sampled_inds

    def assign_targets_to_proposals(self, proposals, gt_boxes, gt_labels):
        matched_idxs = []
        labels = []
        # gt_labels注意一下,是从1开始的,0是背景,这个是由调用者即dataset保证的
        for proposals_in_image, gt_boxes_in_image, gt_labels_in_image in zip(proposals, gt_boxes, gt_labels):

            if gt_boxes_in_image.numel() == 0:
                # Background image
                device = proposals_in_image.device
                clamped_matched_idxs_in_image = torch.zeros(
                    (proposals_in_image.shape[0],), dtype=torch.int64, device=device
                )
                labels_in_image = torch.zeros((proposals_in_image.shape[0],), dtype=torch.int64, device=device)
            else:
                #  set to self.box_similarity when https://github.com/pytorch/pytorch/issues/27495 lands
                match_quality_matrix = box_ops.box_iou(gt_boxes_in_image, proposals_in_image)
                # 通过iou匹配对应的gt框是哪一个,
                # matched_idxs_in_image为对应的gt_boxes.
                # index形如[-1,  0,  1,  2,  3].-1为没匹配到
                matched_idxs_in_image = self.proposal_matcher(match_quality_matrix)

                # 将限制掉-1的idx存一份
                clamped_matched_idxs_in_image = matched_idxs_in_image.clamp(min=0)

                # gt_labels_in_image是一个gt的类型的Tensor,例如[1,3,4,4],clamped_matched_idxs_in_image是索引值,例如torch.Size([2004]),
                # 对gt_labels_in_image求2004个索引对应的值,即得到2004个类型,
                # 按示例:结果labels_in_image其中1理论上应该最多,因为大量的-1被限制为了0,所以定位到了第一个值上,故而是1最多
                # 所以此时labels_in_image是不准确的,并不是每一个proposal都对应到了合适的分类,其中属于背景的proposal被定位到了分类1上
                labels_in_image = gt_labels_in_image[clamped_matched_idxs_in_image]
                labels_in_image = labels_in_image.to(dtype=torch.int64)

                # Label background (below the low threshold)
                # 求出属于背景的index,将labels_in_image错误的设置为分类0(背景).
                bg_inds = matched_idxs_in_image == self.proposal_matcher.BELOW_LOW_THRESHOLD
                labels_in_image[bg_inds] = 0

                # 这里实际是不适用的,因为此处的上下阈值都是0.5,不存在舍弃的数据
                # Label ignore proposals (between low and high thresholds)
                ignore_inds = matched_idxs_in_image == self.proposal_matcher.BETWEEN_THRESHOLDS
                labels_in_image[ignore_inds] = -1  # -1 is ignored by sampler

            # 实际返回的值中labels_in_image是准确的标记了每一个proposal属于哪一个分类(0是背景)
            # matched_idxs是不准确的值,因为使用的是clamped_matched_idxs_in_image,
            # 其中0的很大一部分是属于未匹配到的,而不是第0个gt框,所以使用时需要以labels_in_image为准
            matched_idxs.append(clamped_matched_idxs_in_image)
            labels.append(labels_in_image)
        return matched_idxs, labels


class FastRCNNPredictor(nn.Module):
    def __init__(self, representation_size, num_classes):
        super().__init__()
        self.cls_score = nn.Linear(in_features=representation_size, out_features=num_classes)
        self.bbox_pred = nn.Linear(in_features=representation_size, out_features=num_classes * 4)

    def forward(self, x):
        scores = self.cls_score(x)
        bbox_deltas = self.bbox_pred(x)
        return scores, bbox_deltas


class RPN(nn.Module):
    def __init__(self):
        super().__init__()
        self.anchor_generator = AnchorGenerator()
        self.rpn_head = RpnHead(self.anchor_generator.get_num_anchors())
        # 筛选前2000个,每个层级
        self.top_idx_n = 2000
        # 移除任意一边小于指定值的box
        self.min_size = 1
        # 移除低分候选框
        self.score_thresh = 0.01
        # nms的阈值,大于多少iou的进行nms操作
        self.nms_thresh = 0.7
        # nms后的按得分取多少条(所有层级的)
        self.post_nms_top_n = 2000
        self.training = True
        # iou大于多少的anchor认为是正样本(前景)
        self.fg_iou_thresh = 0.7
        # iou小于多少的anchor认为是负样本(背景)
        self.bg_iou_thresh = 0.3
        # 计算loss的抽样器
        self.fg_bg_sampler = BalancedPositiveNegativeSampler(256, 0.5)

    def forward(self, image_sizes, images, features, targets):
        # 从dict提取值做list
        features = list(features.values())
        """
        获取所有的候选框.返回一个长度为batch_size的list(Tensor(n,4)),n为每个图像的候选框个数(多个层级加起来的)
        输出形如
        [torch.Size([787464, 4]),
        torch.Size([787464, 4]),
        torch.Size([787464, 4]),
        torch.Size([787464, 4])]
        """
        anchors = self.anchor_generator(image_sizes, images, features)
        # 记录一下照片数量,即batch_size
        num_images = len(anchors)
        """
        使用特征图进行预测,获取是对象的概率(objectness),以及边框信息(pred_bbox_deltas). 返回长度为fpn的层级数的list(Tensor(batch_size,k/4k,h,w))
        注意,注意,pred_bbox_deltas并不是直接返回的直接边界值,而是和GT的偏差,因为图像尺寸不同原因,且是参数化后值,故有_deltas后缀
        rpn_head,预测出了所有点的所有框是否为对象,以及距离gt框的偏差
        objectness 形如
        [torch.Size([4, 9, 198, 334]),
        torch.Size([4, 9, 98, 166]),
        torch.Size([4, 9, 48, 82]),
        torch.Size([4, 9, 23, 40]),
        torch.Size([4, 9, 12, 20])]
        pred_bbox_deltas 形如
        [torch.Size([4, 36, 198, 334]),
        torch.Size([4, 36, 98, 166]),
        torch.Size([4, 36, 48, 82]),
        torch.Size([4, 36, 23, 40]),
        torch.Size([4, 36, 12, 20])]
        """
        objectness, pred_bbox_deltas = self.rpn_head(features)
        # 从objectness抽取每张图像每一层有多少个anchor数,提供给filter_proposals使用
        num_anchors_per_level = []
        for obj in objectness:
            sp = obj.shape
            num_anchors_per_level.append(sp[1] * sp[2] * sp[3])
        # 将objectness, pred_bbox_deltas展平为(n,1)和(n,4),不是核心方法,这边直接用库函数了
        objectness, pred_bbox_deltas = concat_box_prediction_layers(objectness, pred_bbox_deltas)
        # 解码,pred_bbox_deltas,因为是预测的结果,不要影响他,故而detach一份
        pred_boxes = self.decode(anchors, pred_bbox_deltas.detach())
        # 将Tensor(3580920,4) -> Tensor(num_images,-1,4) ,即拆分到每个照片中,即得到proposals
        proposals = pred_boxes.reshape(num_images, -1, 4)
        boxes, scores = self.filter_proposals(proposals, objectness.detach(), image_sizes, num_anchors_per_level)

        losses = {}
        # 训练阶段需要计算loss
        if self.training:
            if targets is None:
                raise ValueError("targets should not be None")
            # 将anchor和target的gt_box匹配起来,且确定哪些是正负样本
            labels, matched_gt_boxes = self.assign_targets_to_anchors(anchors, targets)
            # 对gt_boxes(对应后就是:matched_gt_boxes)进行编码
            regression_targets = self.encode(anchors, matched_gt_boxes)
            # 实际计算loss
            loss_objectness, loss_rpn_box_reg = self.compute_loss(
                objectness, pred_bbox_deltas, labels, regression_targets
            )
            losses = {
                "loss_objectness": loss_objectness,
                "loss_rpn_box_reg": loss_rpn_box_reg,
            }
        return boxes, losses

    def compute_loss(self, objectness, pred_bbox_deltas, labels, regression_targets):
        # 采样,取出样本的index,合并
        sampled_pos_inds, sampled_neg_inds = self.fg_bg_sampler(labels)
        sampled_pos_inds = torch.where(torch.cat(sampled_pos_inds, dim=0))[0]
        sampled_neg_inds = torch.where(torch.cat(sampled_neg_inds, dim=0))[0]
        sampled_inds = torch.cat([sampled_pos_inds, sampled_neg_inds], dim=0)

        # 都合并掉image一层然后计算
        objectness = objectness.flatten()
        labels = torch.cat(labels, dim=0)
        regression_targets = torch.cat(regression_targets, dim=0)

        # 回归使用平滑L1损失
        box_loss = nn.functional.smooth_l1_loss(
            pred_bbox_deltas[sampled_pos_inds],
            regression_targets[sampled_pos_inds],
            beta=1 / 9,
            reduction="sum",
        ) / (sampled_inds.numel())

        # 使用交叉熵损失
        objectness_loss = nn.functional.binary_cross_entropy_with_logits(objectness[sampled_inds], labels[sampled_inds])

        return objectness_loss, box_loss

    @staticmethod
    def encode(anchors, gt_boxes):
        # 由于是list传入,先合并为同一tensor,处理后拆分
        boxes_per_image = [len(b) for b in anchors]
        anchors = torch.cat(anchors, dim=0)
        gt_boxes = torch.cat(gt_boxes, dim=0)

        ax1 = anchors[:, 0].unsqueeze(1)
        ay1 = anchors[:, 1].unsqueeze(1)
        ax2 = anchors[:, 2].unsqueeze(1)
        ay2 = anchors[:, 3].unsqueeze(1)

        gx1 = gt_boxes[:, 0].unsqueeze(1)
        gy1 = gt_boxes[:, 1].unsqueeze(1)
        gx2 = gt_boxes[:, 2].unsqueeze(1)
        gy2 = gt_boxes[:, 3].unsqueeze(1)

        # 转中心点表示
        aw = ax2 - ax1
        ah = ay2 - ay1
        acx = ax1 + 0.5 * aw
        acy = ay1 + 0.5 * ah

        gw = gx2 - gx1
        gh = gy2 - gy1
        gcx = gx1 + 0.5 * gw
        gcy = gy1 + 0.5 * gh

        # 计算结果
        tx = (gcx - acx) / aw
        ty = (gcy - acy) / ah
        tw = torch.log(gw / aw)
        th = torch.log(gh / ah)

        # 合并
        targets = torch.cat((tx, ty, tw, th), dim=1)

        return targets.split(boxes_per_image, 0)

    def assign_targets_to_anchors(self, anchors, targets):
        labels = []
        matched_gt_boxes = []
        for anchors_per_image, targets_per_image in zip(anchors, targets):
            gt_boxes = targets_per_image["boxes"]
            # 先判断是gt_boxes否为空,为空,则所有标签都是负样本,所有对应的target都无意义,填充0
            if gt_boxes.numel() == 0:
                matched_gt_boxes_per_image = torch.zeros(anchors_per_image.shape, dtype=torch.float32)
                labels_per_image = torch.zeros((anchors_per_image.shape[0],), dtype=torch.float32)
            else:
                # 计算iou
                iou = box_ops.box_iou(gt_boxes, anchors_per_image)
                # 取出几个gt_box,iou最大的那个,matched_vals为值, matches是index
                matched_vals, matches = iou.max(dim=0)
                # 依据设定阈值,将iou分为3个等级,最高的max > 0.7(正样本),中间的0.7>mid>0.3(丢弃,记作-2),最小的 min<0.3(负样本,记作-1)
                min_match = matched_vals < self.bg_iou_thresh
                mid_match = (matched_vals >= self.bg_iou_thresh) & (matched_vals < self.fg_iou_thresh)
                matches[min_match] = -1
                matches[mid_match] = -2
                # 此时的matches去重后应该形如[-2,-1,0,1,2],其中 -2,-1占绝大多数(假设这3个对象都是小对象),代表的是没匹配到足够合适的GT框.
                # 其中 0,1,2,代表匹配到的gt框的index值. 再依照matches的结果,计算matched_gt_boxes_per_image,labels_per_image
                # matched_gt_boxes_per_image,matches负数代表的是没匹配到足够合适的GT框.
                # 可以直接对应0号gt_box(pytorch这样做的),反正也不用,或者填充0都行(没意义)
                # 其中 0,1,2,代表匹配到的gt框的index值,直接对应好了
                matched_gt_boxes_per_image = gt_boxes[matches.clamp(min=0)]
                # 将0,1,2 转为表示正样本的 1 ,将 -1 转为 表示负样本的 0 ,将 -2 转为表示丢弃的 -1
                labels_per_image = matches >= 0
                labels_per_image = labels_per_image.to(torch.float32)
                labels_per_image[matches == -1] = 0
                labels_per_image[matches == -2] = -1
            labels.append(labels_per_image)
            matched_gt_boxes.append(matched_gt_boxes_per_image)
        return labels, matched_gt_boxes

    def filter_proposals(self, proposals, objectness, image_sizes, num_anchors_per_level):
        """
        过滤proposals
        Args:
            proposals: Tensor,形如:torch.Size([4, 268569, 4])
            objectness: Tensor,形如:torch.Size([1074276, 1])
            image_sizes: list[Tuple[int, int]],形如:[(788, 1333), (800, 1225), (658, 1333), (800, 920)],用于限制超范围的proposals
            num_anchors_per_level: list[int],形如:[201600, 50400, 12600, 3150, 819] 用以筛选objectness前k条,以及index序号用
        """
        # 先同一objectness格式
        num_images = len(image_sizes)
        objectness = objectness.reshape(num_images, -1)

        # 由于nms需要每个框的类别索引,故构建levels
        levels = [
            torch.full((n,), idx, dtype=torch.int64) for idx, n in enumerate(num_anchors_per_level)
        ]
        levels = torch.cat(levels, 0)
        levels = levels.reshape(1, -1).expand_as(objectness)

        # 按层级切分,每个层级按objectness取前self.top_idx_n个数据
        top_n_idxs = []
        offset = 0
        for ob in objectness.split(num_anchors_per_level, 1):
            num_anchors = ob.shape[1]
            _, top_n_idx = ob.topk(min(self.top_idx_n, num_anchors), dim=1)
            top_n_idxs.append(top_n_idx + offset)
            offset += num_anchors
        top_n_idxs = torch.cat(top_n_idxs, dim=1)

        # 从数据中提取指定idx的
        image_range = torch.arange(num_images)
        batch_idx = image_range[:, None]
        objectness = objectness[batch_idx, top_n_idxs]
        proposals = proposals[batch_idx, top_n_idxs]
        levels = levels[batch_idx, top_n_idxs]
        # 由于是2分类问题,直接做sigmoid作为概率
        objectness_prob = torch.sigmoid(objectness)

        # 保存结果,每条记录是每个照片筛选后的proposals
        final_boxes = []
        final_scores = []
        for boxes, scores, lvl, image_size in zip(proposals, objectness_prob, levels, image_sizes):
            # 将proposals限制到图像范围内
            box_ops.clip_boxes_to_image(boxes, image_size)
            # 移除小候选框
            keep = box_ops.remove_small_boxes(boxes, self.min_size)
            boxes, scores, lvl = boxes[keep], scores[keep], lvl[keep]
            # 移除低分候选框
            keep = torch.where(scores >= self.score_thresh)[0]
            boxes, scores, lvl = boxes[keep], scores[keep], lvl[keep]
            # nms非极大值抑制(每一级别),返回值是按得分降序的索引
            keep = box_ops.batched_nms(boxes, scores, lvl, self.nms_thresh)

            keep = keep[: self.post_nms_top_n]
            boxes, scores = boxes[keep], scores[keep]
            final_boxes.append(boxes)
            final_scores.append(scores)
        return final_boxes, final_scores

    @staticmethod
    def decode(anchors, rel_codes):
        """
        对RpnHead的pred_bbox_deltas进行解码操作,得到真的预测框
        """
        concat_boxes = torch.cat(anchors, dim=0)

        max_clip = math.log(62.5)
        # 2点表示转中心宽高
        w = concat_boxes[:, 2] - concat_boxes[:, 0]
        h = concat_boxes[:, 3] - concat_boxes[:, 1]
        cx = concat_boxes[:, 0] + 0.5 * w
        cy = concat_boxes[:, 1] + 0.5 * h

        # 获取偏移dx,dy,dw,dh.即0,1,2,3位置,(可能存在权重,如果存在就要除权重)
        dx = rel_codes[:, 0]
        dy = rel_codes[:, 1]
        dw = rel_codes[:, 2]
        dh = rel_codes[:, 3]

        # 限制偏移再范围内
        dw = torch.clamp(dw, max=max_clip)
        dh = torch.clamp(dh, max=max_clip)

        # 计算中心宽高表示的预测框,p表示predict
        pcx = dx * w + cx
        pcy = dy * h + cy
        pw = torch.exp(dw) * w
        ph = torch.exp(dh) * h

        # 中心宽高换2点表示
        pw2 = pw / 2
        ph2 = ph / 2
        px1 = pcx - pw2
        py1 = pcy - ph2
        px2 = pcx + pw2
        py2 = pcy + ph2
        # 合并tensor
        pred_boxes = torch.stack((px1, py1, px2, py2), dim=1)
        return pred_boxes


class AnchorGenerator(nn.Module):
    def __init__(self):
        super().__init__()
        # size的长度必须和层级数量一致,每一个size[i]的长度也必须一致
        self.sizes = ((8, 16, 32), (16, 32, 64), (32, 64, 128), (64, 128, 256), (256, 512, 1024))
        # self.sizes = ((8,), (16,), (32,), (64,), (256,))
        self.aspect_ratios = ((0.5, 1.0, 2.0),) * len(self.sizes)
        self.device = torch.device("cpu")

    def get_num_anchors(self):
        """
        计算获取每个点有多少个候选框
        """
        return len(self.sizes[0]) * len(self.aspect_ratios[0])

    def forward(self, image_sizes, images, features):
        """
        image_sizes resize后的原始图像大小
        images 合并后统一的图像Tensor(4,3,800,1344)
        features fpn的特征图
        """
        # 上获取合并后统一的图像Tensor(4,3,800,1344)大小,即 800x1344
        # 分别除以特征图每一个层级的大小,得到每一层级的缩放比例strides
        strides = []
        grid_sizes = [feather.shape[-2:] for feather in features]
        image_size = images.shape[-2:]
        for g in grid_sizes:
            stride_x = torch.empty((), dtype=torch.int64, device=self.device).fill_(image_size[0] // g[0])
            stride_y = torch.empty((), dtype=torch.int64, device=self.device).fill_(image_size[1] // g[1])
            strides.append([stride_x, stride_y])

        # 创建所有层级的cell_anchors
        # 获取cell_anchors(9,4) 表示每个中心点都有9中可能,坐标距离(x1,y1,x2,y2)偏移分别是(a1,b1,a2,b2)(例如:[-11.,  -6.,  11.,   6.])
        cell_anchors = [self.generate_anchors(size, aspect_ratio) for size, aspect_ratio in
                        zip(self.sizes, self.aspect_ratios)]
        """
        存放每个层级计算好的anchor坐标,形如
        [torch.Size([595188, 4]),
        torch.Size([146412, 4]),
        torch.Size([35424, 4]),
        torch.Size([8280, 4]),
        torch.Size([2160, 4])]
        """
        anchors_over_all_feature_maps: list[torch.Tensor] = []
        # 计算不同尺度下的anchor,然后合并
        for stride, cell_anchor, feather in zip(strides, cell_anchors, features):
            grid_height, grid_width = feather.shape[-2:]
            # 和原图的缩放比例
            stride_height, stride_width = stride
            # 依照缩放比例,即步幅,将特征图像素点缩放到原始图上
            shifts_x = torch.arange(0, grid_width, dtype=torch.int32, device=self.device) * stride_width
            shifts_y = torch.arange(0, grid_height, dtype=torch.int32, device=self.device) * stride_height
            # 将(200x272)=54400个中心点数据(cx,cy) 变为54400x4的数据,即54400x(x1,y1,x2,y2)=(cx,cy,cx,cy)
            shift_y, shift_x = torch.meshgrid(shifts_y, shifts_x, indexing="ij")
            shift_x = shift_x.reshape(-1)
            shift_y = shift_y.reshape(-1)
            # shifts表示共有54400个中心点,每个中心点的坐标为(x1,y1,x2,y2);
            shifts = torch.stack((shift_x, shift_y, shift_x, shift_y), dim=1)

            # 只需要将shifts(54400x4) + cell_anchors(9x4)相加即可的得到结果(489600x4),即shifts,cell_anchors的第一维分别扩展9和54400倍即可
            anchor = shifts.view(-1, 1, 4) + cell_anchor.view(1, -1, 4)
            # 将 54400x9x4 转为 489600x4 ,代表共有489600的anchor,每个含1个坐标(x1,y1,x2,y2)
            anchor = anchor.view(-1, 4)
            anchors_over_all_feature_maps.append(anchor)

        # 合并到Tensor中的图像具有相同的大小。
        # anchors_over_all_feature_maps是图像的anchor。对于批次上的所有图像，anchor应该是一致的。因此，多次复制图像以对应于不同的图像
        anchors = []
        for _ in range(len(image_sizes)):
            anchors_in_image = [anchors_per_feature_map for anchors_per_feature_map in anchors_over_all_feature_maps]
            anchors.append(anchors_in_image)
        anchors = [torch.cat(anchors_per_image) for anchors_per_image in anchors]
        return anchors

    @staticmethod
    def generate_anchors(
            scales,
            aspect_ratios,
            dtype: torch.dtype = torch.float32,
            device: torch.device = torch.device("cpu"),
    ) -> Tensor:
        """
        创建单个层级的基础框,形如.注意,需要为每一个层级都创建一个
        tensor([[-11.,  -6.,  11.,   6.],
        [-23., -11.,  23.,  11.],
        [-45., -23.,  45.,  23.],
        [ -8.,  -8.,   8.,   8.],
        [-16., -16.,  16.,  16.],
        [-32., -32.,  32.,  32.],
        [ -6., -11.,   6.,  11.],
        [-11., -23.,  11.,  23.],
        [-23., -45.,  23.,  45.]])
        """
        scales = torch.as_tensor(scales, dtype=dtype, device=device)
        aspect_ratios = torch.as_tensor(aspect_ratios, dtype=dtype, device=device)
        h_ratios = torch.sqrt(aspect_ratios)
        w_ratios = 1 / h_ratios

        ws = (w_ratios[:, None] * scales[None, :]).view(-1)
        hs = (h_ratios[:, None] * scales[None, :]).view(-1)

        base_anchors = torch.stack([-ws, -hs, ws, hs], dim=1) / 2
        return base_anchors.round()


class RpnHead(nn.Module):
    def __init__(self, num_anchors: int):
        super().__init__()
        self.conv = Conv2dNormActivation(256, 256, kernel_size=3,
                                         norm_layer=None, activation_layer=None)
        self.cls_conv = nn.Conv2d(256, num_anchors, kernel_size=1, stride=1)
        self.bbox_conv = nn.Conv2d(256, num_anchors * 4, kernel_size=1, stride=1)
        # 初始化参数
        for layer in self.modules():
            if isinstance(layer, nn.Conv2d):
                torch.nn.init.normal_(layer.weight, std=0.01)  # type: ignore[arg-type]
                if layer.bias is not None:
                    torch.nn.init.constant_(layer.bias, 0)  # type: ignore[arg-type]

    def forward(self, features):
        cls = []
        bbox = []
        # 对每一个层级的特征图执行卷积3x3卷积,再计算分类和回归.
        # 注意是共享卷积核,即所有的层级,都使用同一卷积层处理
        for x in features:
            x = self.conv(x)
            bbox.append(self.bbox_conv(x))
            cls.append(self.cls_conv(x))
        return cls, bbox


class Maskrcnn(nn.Module):
    def __init__(self):
        super().__init__()
        self.transform = TransformRcnn()
        self.fpn = ResNet50WithFpn()
        self.rpn = RPN()
        self.roi_heads = RoIHeads(5)

    def forward(self, images, targets=None):
        # transform,处理图像,得到图像尺寸,以及tensor化的images
        image_sizes, images, targets = self.transform(images, targets)
        # 从图像提取fpn层
        features = self.fpn(images)
        # 构建rpn网络,计算proposal(即ROI),以及rpn的loss
        proposals, proposal_losses = self.rpn(image_sizes, images, features, targets)
        # 计算预测结果,以及预测loss
        detections, detector_losses = self.roi_heads(features, proposals, image_sizes, targets)

        # 组合rpn和roiHeads的loss
        losses = {}
        losses.update(detector_losses)
        losses.update(proposal_losses)

        if self.training:
            return losses
        return detections
